﻿using System;

namespace CodeBuildDemo
{
    public class DumbFunctions
    {

        public double FinalPrice(double price, string coupon)
        {
            double discount = 0;
            if (coupon != "")
            {
                discount = CouponDiscount(coupon);
            }

            return price - (price * discount);

        }

        private double CouponDiscount(string coupon)
        {
            if (coupon == "40less")
            {
                return 0.40;
            }

            return 0;
        }
        
    }
}