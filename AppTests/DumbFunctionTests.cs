using System;
using CodeBuildDemo;
using Xunit;

namespace AppTests
{
    public class DumbFunctionTests
    {
        [Fact]
        public void PriceWithNoDiscount()
        {
            var sut = new DumbFunctions();

            var price = sut.FinalPrice(100, "");
            
            Assert.Equal(100, price);
        }

        [Fact]
        public void PriceWithNonExistentCoupon()
        {
            var sut = new DumbFunctions();

            var price = sut.FinalPrice(100, "DescuentoDeLocos");

            Assert.Equal(100, price);
        }
        
        [Fact]
        public void PriceWithNonExistentCouponFail()
        {
            var sut = new DumbFunctions();

            var price = sut.FinalPrice(100, "DescuentoDeLocos");

            Assert.Equal(100, price);
        }
    }
}

